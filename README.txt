Partner 1's name:  Ming-Yee Tsang 
Partner 1's login: mtsang

(Leave blank if you are working alone)
Partner 2's name:  Alex Joo
Partner 2's login: ajoo



Problems or Issues or Difficulties with the Assignment:
-------------------------------------------------------
Really confusing at the beginning, when trying to figure out how the various
parts relate to each other. There is a lot of interdependency and cross-file
referencing that, if their relationships had been made lucid from the start
would have spared us much back-and-forth scouring of files to try and make
sense of the maelstrom of modules and functions.

Suggestions for the Future or Random Comments:
----------------------------------------------
Address the above problem, I suppose?
