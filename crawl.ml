open Util ;;    
open CrawlerServices ;;
open Order ;;
open Pagerank ;;



let debug = false

module MoogleRanker
  (* = InDegreeRanker (PageGraph) (PageScore) *)
     = RandomWalkRanker (PageGraph) (PageScore) (struct 
       let do_random_jumps = Some 0.20
       let num_steps = 1000
     end)

(* Dictionaries mapping words (strings) to sets of crawler links *)
module WordDict = Dict.Make(
  struct 
    type key = string
    type value = LinkSet.set
    let compare = string_compare
    let string_of_key = (fun s -> s)
    let string_of_value = LinkSet.string_of_set

    (* These functions are for testing purposes *)
    let gen_key ()             = ""
    let gen_key_gt x ()        = gen_key ()
    let gen_key_lt x ()        = gen_key ()
    let gen_key_random ()      = gen_key ()
    let gen_key_between x y () = None
    let gen_value ()           = LinkSet.empty
    let gen_pair ()            = (gen_key(), gen_value())
  end)

(* A query module that uses LinkSet and WordDict *)
module Q = Query.Query(
  struct
    module S = LinkSet
    module D = WordDict
  end)

let print s = 
  let _ = Printf.printf "%s\n" s in
  flush_all();;


(***********************************************************************)
(*    PART 1: CRAWLER                                                  *)
(***********************************************************************)

(* Build an index as follows:
 * 
 * Remove a link from the frontier (the set of links that have yet to
 * be visited), visit this link, add its outgoing links to the
 * frontier, and update the index so that all words on this page are
 * mapped to LinkSets containing this url.
 *
 * Keep crawling until we've
 * reached the maximum number of links (n) or the frontier is empty. *)
let rec crawl (n:int) (frontier: LinkSet.set)
    (visited : LinkSet.set) (d:WordDict.dict) : WordDict.dict =
  if n <= 0 then d
  else match LinkSet.choose frontier with
  | None         -> d (* frontier is empty *)
  | Some (x, xs) ->   (* NOTE: x is a link *)
    if LinkSet.member visited x then crawl n xs visited d
    else (
      match get_page x with
      | None   -> crawl n xs visited d
      | Some p -> (
        let (words, outgoing) = (p.words, p.links) in
        (* XXX: Pasted in LinkSet.insert_list def here
         *      b/c of unbound value error *)
        let nfrontier = List.fold_left
            (fun r k -> LinkSet.insert k r) xs outgoing in
        let ndict = List.fold_left (fun currdict word -> 
          match WordDict.lookup currdict word with
          | None        -> WordDict.insert currdict word
                             (LinkSet.singleton x)
          | Some lnkset -> WordDict.insert currdict word
                             (LinkSet.insert x lnkset)
        ) d words in
        let nvisited = LinkSet.insert x visited in
        if debug then (
          Printf.printf "%d : %s\n" n (string_of_link x);
          Printf.printf "%s\n" (LinkSet.string_of_set nfrontier)
        );
        crawl (n-1) nfrontier nvisited ndict
      )
    )
;;

let crawler () = 
  crawl num_pages_to_search (LinkSet.singleton initial_link) LinkSet.empty
    WordDict.empty
;;

(* Debugging note: if you set debug=true in moogle.ml, it will print out your
 * index after crawling. *)
